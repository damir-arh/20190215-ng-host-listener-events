import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild('form')
  form: ElementRef;

  @ViewChild('submitButton')
  submitButton: ElementRef;

  clickSubmit() {
    this.submitButton.nativeElement.click();
  }

  submitForm() {
    this.form.nativeElement.submit();
  }

  dispatchEvent() {
    const event = new Event('submit', { bubbles: true, cancelable: true });
    if (this.form.nativeElement.dispatchEvent(event)) {
      this.form.nativeElement.submit();
    }
  }
}
