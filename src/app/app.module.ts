import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InterceptSubmitDirective } from './intercept-submit.directive';

@NgModule({
  declarations: [
    AppComponent,
    InterceptSubmitDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
