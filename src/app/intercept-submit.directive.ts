import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appInterceptSubmit]'
})
export class InterceptSubmitDirective {

  constructor() { }

  @HostListener('submit', ['$event'])
  onSubmit(event: Event) {
    console.log('Form submitted!');
    event.preventDefault();
  }
}
